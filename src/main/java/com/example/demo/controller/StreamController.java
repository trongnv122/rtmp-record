package com.example.demo.controller;

import com.example.demo.dto.request.RecordRequest;
import com.example.demo.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StreamController {
    private final RecordService recordService;

    public StreamController(RecordService recordService) {
        this.recordService = recordService;
    }

    @PostMapping(value = "/record")
    public ResponseEntity<Boolean> listen(@RequestBody RecordRequest request) {
        return ResponseEntity.ok(recordService.record(request));
    }
}
