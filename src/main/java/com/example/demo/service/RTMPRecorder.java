package com.example.demo.service;

import com.example.demo.dto.request.RecordRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class RTMPRecorder implements Runnable {
    private final String RTMP_URL;
    private final String OUTPUT_DIR;
    // Time in seconds
    private final Integer MAX_SECOND_TIME;
    private final RecordRequest request;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public RTMPRecorder(Environment env, RecordRequest request) {
        RTMP_URL = env.getProperty("RTMP_URL");
        OUTPUT_DIR = env.getProperty("OUTPUT_DIR");
        String MAX_SECOND_STR = env.getProperty("MAX_TIME");
        MAX_SECOND_TIME = MAX_SECOND_STR == null ? 120 : Integer.parseInt(MAX_SECOND_STR);
        this.request = request;
    }

    @Override
    public void run() {
        String id = new SimpleDateFormat("yyyy_MM_dd_hh_ss_").format(new Date()) + UUID.randomUUID();
        id = id.replaceAll("-", "_");
        try {
            logger.info("Start record with id: {}, url: {}, channel {}, seconds: {} out dir: {}",
                    id, RTMP_URL, request.channel, request.minutes, OUTPUT_DIR);

            String timeout = String.valueOf(MAX_SECOND_TIME);
            if (request.minutes != null) {
                timeout = String.valueOf(Math.min(request.minutes * 60, MAX_SECOND_TIME));
            }

            String fileID = OUTPUT_DIR;
            if (System.getProperty("os.name").toLowerCase().contains("win") && !fileID.endsWith("\\")) {
                fileID += "\\";
            } else if (!fileID.endsWith("/")) {
                fileID += "/";
            }

            fileID += id + ".mp4";

            // High quality ~ 1080
            //"ffmpeg -i input.mp4 -c:v libx264 -preset slow -crf 18 -maxrate 4M -bufsize 8M -vf "scale=-2:1080" -c:a aac -b:a 256k output.mp4"

            String streamURL =  (RTMP_URL.endsWith("/") ? RTMP_URL : RTMP_URL + "/") + request.channel;

            String[] command = {"ffmpeg", "-i", streamURL, "-c:v", "libx264", "-preset", "slow", "-crf", "18",
                    "-maxrate", "4M", "-bufsize", "8M", "-c:a", "aac", "-b:a", "256k",
                    "-max_muxing_queue_size", "9999",
                    "-t", timeout, fileID};

            ProcessBuilder pb = new ProcessBuilder(command);
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor();

            logger.info("Record done with id: {}", id);
        } catch (Exception ex) {
            logger.error("Record error with id: {}", id, ex);
        }
    }
}
