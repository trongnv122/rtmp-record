package com.example.demo.service;

import com.example.demo.dto.request.RecordRequest;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class RecordService {
    public RecordService(Environment env) {
        this.env = env;
    }

    final ExecutorService executorService = Executors.newFixedThreadPool(3);

    private final Environment env;

    public boolean record(RecordRequest request) {
        executorService.submit(new RTMPRecorder(env, request));
        return true;
    }
}
