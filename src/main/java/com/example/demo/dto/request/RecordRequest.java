package com.example.demo.dto.request;

public class RecordRequest {
    public Integer minutes;
    public String filename;
    public String channel;
}
